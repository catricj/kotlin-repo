# Kotlin Repo
## Jacob Catricala

### Repo
This Repo is designed to host all files associated with Jacob Catricala's Final Project for CS490 Kotlin Programming.
This Repository contains the source code for the application, as well as an APK of the current build of the application.

### Objective
This Project's Objective will be to create a Game using Android Libraries.
The Game will consist of tapping highlighted portions of the app window in a specified amount of time to recieve points.
The amount of time will decrease proportionally to how many points the user has.
The user loses if they do not tap the correct part of the screen, or does not tap it within the time limit.

### Outcome
This Project was able to fulfill its main objective to create a game as an Android application using Kotlin and Android Libraries.
Some features of the app were completed, while others were not or could not be.
The application does allow the user to accrue points by tapping a highlighted circle on the screen.
If the user does not tap the circle on the screen, they will lose a set amount of points.
Due to the nature of the android.CountDownTimer class, the timer could not have been made mutable in its original function.
The project, therefore, has a timer that can be changed in the source code.
I am not currently able to have it running on my personal Android phone, and I was not able to test on any physical phone other than the virtual systems provided to me via Android Studio. My theory is this is because hardware accceleration may be neccesary to run the application, and will crash otherwise.
The application is designed primarily for Android phones running Android OS 7.1.1 Nougat, and will not work on systems older than Android OS 4.1 Jelly Bean.
The application should be able to scale to different resolutioned phones as well. The application is desidned for use in Portrait mode.