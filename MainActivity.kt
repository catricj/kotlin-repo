package com.example.kotlintapv2

import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import java.util.concurrent.ThreadLocalRandom

class MainActivity : AppCompatActivity() {
    var score: Int = 0
    var highScore: Int = 0

    var handler: Handler = Handler()
    var runnable: Runnable = Runnable { }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        score = 0

        val scoreText: TextView = findViewById(R.id.scoreText)
        scoreText.visibility = View.VISIBLE
        val titleText: TextView = findViewById(R.id.titleText)
        titleText.visibility = View.VISIBLE
        val highScoreText: TextView = findViewById(R.id.highScoreText)
        highScoreText.visibility = View.VISIBLE
        val timerText: TextView = findViewById(R.id.timerText)
        timerText.visibility = View.INVISIBLE
        val authorText: TextView = findViewById(R.id.authorText)
        authorText.visibility = View.VISIBLE
        val startButton: Button = findViewById(R.id.StartButton)
        startButton.visibility = View.VISIBLE
        val circle: ImageView = findViewById(R.id.circle)
        circle.visibility = View.GONE
        val decreaseTimerButton: ImageView = findViewById(R.id.decreaseTimerButton)
        decreaseTimerButton.visibility = View.GONE

        startButton.setOnClickListener() {
            runnable = Runnable {
                score = 0
                scoreText.setText("Score: " + score)
                circle.visibility = View.VISIBLE

                startButton.visibility = View.GONE
                decreaseTimerButton.bringToFront()
                circle.bringToFront()

                object : CountDownTimer(10000, 1000) {
                    override fun onFinish() {
                        timerText.visibility = View.INVISIBLE
                        startButton.visibility = View.VISIBLE
                        circle.visibility = View.INVISIBLE

                        handler.removeCallbacks(runnable)
                    }

                    override fun onTick(time: Long) {
                        timerText.setText((time/1000).toString())
                        circle.visibility = View.VISIBLE
                        decreaseTimerButton.visibility = View.VISIBLE
                        circle.bringToFront()
                    }
                }.start()

                timerText.visibility = View.VISIBLE
                decreaseTimerButton.visibility = View.INVISIBLE
            }
            handler.post(runnable)
        }

        circle.setOnClickListener() {
            score += 10
            scoreText.setText("Score: " + score)

            highScore = if (score > highScore) {
                score
            } else {
                highScore
            }
            highScoreText.setText("High Score: " + highScore)
            moveCircle(circle)
        }

        decreaseTimerButton.setOnClickListener() {
            score -= 20
            scoreText.setText("Score: " + score)
        }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun moveCircle(circle: ImageView) {
        val newX: Int = ThreadLocalRandom.current().nextInt(0, 800)
        val newY: Int = ThreadLocalRandom.current().nextInt(300, 1200)
        circle.x = newX.toFloat()
        circle.y = newY.toFloat()
        circle.bringToFront()
    }
}